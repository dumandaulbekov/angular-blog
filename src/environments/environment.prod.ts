export const environment = {
  production: true,
  auth_url: `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=`,
  db_url: 'https://angular-blog-59314.firebaseio.com',
};
