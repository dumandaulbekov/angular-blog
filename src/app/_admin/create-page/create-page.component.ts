import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Post } from 'src/app/_shared/interfaces';
import { PostService } from 'src/app/_shared/services/post.services';
import { AlertService } from '../_shared/services/alert.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss'],
})
export class CreatePageComponent implements OnInit {
  public form: FormGroup;

  constructor(private alert: AlertService, private postService: PostService) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(null, Validators.required),
      content: new FormControl(null, Validators.required),
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const username = localStorage.getItem('user').split('@');
    const post: Post = {
      title: this.form.value.title,
      content: this.form.value.content,
      author: username[0],
      date: new Date(),
    };

    this.postService.create(post).subscribe((v) => {
      this.form.reset();
      this.alert.success('Post created');
      console.log('v', v);
    });

    console.log('post', post);
  }
}
