import { Pipe, PipeTransform } from '@angular/core';
import { Post } from 'src/app/_shared/interfaces';

@Pipe({
  name: 'serachPost',
})
export class SerachPost implements PipeTransform {
  transform(posts: Post[], serach = ''): Post[] {
    if (!serach.trim()) {
      return posts;
    }

    return posts.filter((post) => {
      return post.title.toLowerCase().includes(serach.toLowerCase());
    });
  }
}
