import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User, AuthResponse } from 'src/app/_shared/interfaces';
import { Observable, throwError, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root'})
export class AuthService {
  public error$: Subject<string> = new Subject<string>();

  private API_KEY = 'AIzaSyDhofMMmybX5zUmkPb5IHLb_QhG67C87TA';

  constructor(private http: HttpClient) {}

  public get token(): string {
    const expDate = new Date(localStorage.getItem('fb-token-exp'));
    if (new Date() > expDate) {
      this.logout();
      return null;
    }

    return localStorage.getItem('fb-token');
  }

  public login(user: User): Observable<any> {
    return this.http
      .post(environment.auth_url + this.API_KEY, user)
      .pipe(tap(this.setToken), catchError(this.handleError.bind(this)));
  }

  public logout() {
    this.setToken(null);
  }

  public isAuth(): boolean {
    return !!this.token;
  }

  private handleError(error: HttpErrorResponse) {
    const { message } = error.error.error;

    switch (message) {
      case 'EMAIL_NOT_FOUND':
        this.error$.next('not found this email');
        break;
      case 'INVALID_EMAIL':
        this.error$.next('invalid email');
        break;
      case 'INVALID_PASSWORD':
        this.error$.next('invalid password');
        break;
      default:
        break;
    }

    return throwError(error);
  }

  private setToken(response: AuthResponse | null) {
    if (response) {
      const expDate = new Date(
        new Date().getTime() + +response.expiresIn * 1000
      );
      localStorage.setItem('fb-token', response.idToken);
      localStorage.setItem('fb-token-exp', expDate.toString());
      localStorage.setItem('user', response.email);
    } else {
      localStorage.clear();
    }
  }
}
