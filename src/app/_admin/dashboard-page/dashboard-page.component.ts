import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from 'src/app/_shared/services/post.services';
import { Post } from 'src/app/_shared/interfaces';
import { Subscription } from 'rxjs';
import { AlertService } from '../_shared/services/alert.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent implements OnInit, OnDestroy {
  public posts: Post[] = [];
  public searchStr = '';

  private postSub: Subscription;
  private deleteSub: Subscription;

  constructor(private alert: AlertService, private postService: PostService) {}

  ngOnInit(): void {
    this.postSub = this.postService.getAll().subscribe((posts) => {
      this.posts = posts;
    });
  }

  public remove(id: string) {
    this.deleteSub = this.postService.remove(id).subscribe(() => {
      this.posts = this.posts.filter((post) => post.id !== id);
      this.alert.danger('Post deleted');
    });
  }

  ngOnDestroy() {
    if (this.postSub) {
      this.postSub.unsubscribe();
    }

    if (this.deleteSub) {
      this.deleteSub.unsubscribe();
    }
  }
}
