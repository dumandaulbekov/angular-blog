import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from 'src/app/_shared/services/post.services';
import { ActivatedRoute, Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Post } from 'src/app/_shared/interfaces';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AlertService } from '../_shared/services/alert.service';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
})
export class EditPageComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public submitted = false;

  private post: Post;
  private updateSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private alert: AlertService
  ) {}

  ngOnInit(): void {
    this.route.params
      .pipe(
        switchMap((params: Params) => {
          return this.postService.getById(params[`id`]);
        })
      )
      .subscribe((post: Post) => {
        this.post = post;
        this.form = new FormGroup({
          title: new FormControl(post.title, Validators.required),
          content: new FormControl(post.content, Validators.required),
        });
      });
  }

  public submit() {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true;
    this.updateSub = this.postService
      .update({
        ...this.post,
        title: this.form.value.title,
        content: this.form.value.content,
      })
      .subscribe(() => {
        this.submitted = false;
        this.alert.success('Post updated');
      });
  }

  ngOnDestroy() {
    if (this.updateSub) {
      this.updateSub.unsubscribe();
    }
  }
}
