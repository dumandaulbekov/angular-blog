import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginPageComponent } from './login-page/login-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { CreatePageComponent } from './create-page/create-page.component';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';

import { AuthService } from './_shared/services/auth.services';
import { SharedModule } from '../_shared/shared.module';
import { AuthGuard } from './_shared/services/auth.guard';
import { SerachPost } from './_shared/search.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertService } from './_shared/services/alert.service';

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      { path: '', redirectTo: '/admin/login', pathMatch: 'full' },
      { path: 'login', component: LoginPageComponent },
      {
        path: 'dashboard',
        component: DashboardPageComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'create',
        component: CreatePageComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'post/:id/edit',
        component: EditPageComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  declarations: [
    LoginPageComponent,
    DashboardPageComponent,
    EditPageComponent,
    CreatePageComponent,
    AdminLayoutComponent,
    SerachPost,
    AlertComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  providers: [AuthGuard, AlertService],
})
export class AdminModule {}
